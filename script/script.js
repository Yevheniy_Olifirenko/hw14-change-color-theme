const themes = ["light-theme", "dark-theme"];
const btn = document.querySelector(".btn-change");
const activeTheme = localStorage.getItem("theme");

btn.addEventListener("click", changeTheme);

function changeTheme() {
    const body = document.body;
    const currentTheme = body.classList[0];
    const nextTheme = themes.find(theme => theme !== currentTheme);

    saveTheme(nextTheme);
    saveLocalStorage(nextTheme);
};

function saveTheme(theme) {
    document.body.className = theme;
};

function saveLocalStorage(theme) {
    localStorage.setItem("theme", theme);
};

if (activeTheme && themes.includes(activeTheme)) {
    saveTheme(activeTheme);
} else {
    saveTheme("light-theme");
};